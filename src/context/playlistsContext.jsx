import React, { useReducer } from 'react';
import { playlistsReducer } from '../reducer/playlistsReducer';

export const PlaylistsContext = React.createContext();

export const PlaylistsProvider = ({ children }) => {
  // retrieve playlists from localStorage when available
  const getPlaylists = () =>
    localStorage.getItem('playlists')
      ? JSON.parse(localStorage.getItem('playlists')).playlists
      : [];

  const [playlistsState, playlistsDispatch] = useReducer(playlistsReducer, {
    playlists: getPlaylists(),
  });

  return (
    <PlaylistsContext.Provider
      value={{
        playlistsState,
        playlistsDispatch,
      }}
    >
      {children}
    </PlaylistsContext.Provider>
  );
};
