import React, { useContext } from 'react';
import { PlaylistsContext, PlaylistsProvider } from '../playlistsContext';
import { mount } from 'enzyme';

describe('playlists', () => {
  it('Adds playlists', () => {
    const MockComponent = () => {
      const { playlistsState, playlistsDispatch } = useContext(PlaylistsContext);

      return (
        <>
          <div data-testid="playlistLength">{playlistsState.playlists.length}</div>
          <button
            type="button"
            onClick={
              () => playlistsDispatch({
                type: 'ADD_PLAYLIST',
                payload: 'playlistName',
              })
            }
          >
            Add
          </button>
        </>
      );
    };

    const wrapper = mount(
      <PlaylistsProvider>
        <MockComponent />
      </PlaylistsProvider>
    );

    expect(wrapper.find('[data-testid="playlistLength"]').text()).toEqual('0');
    wrapper.find('button').simulate('click');
    expect(wrapper.find('[data-testid="playlistLength"]').text()).toEqual('1');
    wrapper.find('button').simulate('click');
    wrapper.find('button').simulate('click');
    expect(wrapper.find('[data-testid="playlistLength"]').text()).toEqual('3');
  });
});
