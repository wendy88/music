// Quick fix considering the available time is running out
// TODO needs fixing later
const getUniqueId = name => `${name.trim().replace(/\s/g, '%20')}${Math.random().toString(36).substring(7)}`;

export const playlistsReducer = (state, action) => {
  switch (action.type) {
    case 'ADD_PLAYLIST': {
      const { payload } = action;
      const newPlayList = {
        ...state,
        playlists: [...state.playlists, { name: payload, id: getUniqueId(payload) }],
      };

      localStorage.setItem('playlists', JSON.stringify(newPlayList));

      return newPlayList;
    }
    case 'REMOVE_PLAYLIST': {
      const newPlaylist = {
        ...state,
        playlists: state.playlists.filter(
          (playlists) => playlists.id !== action.payload
        )
      };

      localStorage.setItem('playlists', JSON.stringify(newPlaylist));

      return newPlaylist;
    }
    default: {
      return state;
    }
  }
};
