import React from 'react';
import Artists from '../Artists/artists';

const Home = () => (
  <div>
    <Artists />
  </div>
);

export default Home;
