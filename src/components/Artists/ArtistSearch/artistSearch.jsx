import React, { useCallback, useState } from 'react';
import styles from './artistSearch.module.css';

const ArtistSearch = ({ setArtistQuery }) => {
  const [artistInput, setArtistInput] = useState('');

  const handleArtistChange = useCallback((e) => {
    setArtistInput(e.target.value);
  }, [setArtistInput]);

  const submitArtistSearch = useCallback((e) => {
    e.preventDefault();
    setArtistQuery(artistInput);
  }, [artistInput]);

  return (
    <form
      onSubmit={(e) => submitArtistSearch(e)}
      className={styles.artistSearch}
    >
      <label htmlFor="artistSearch" className={styles.label}>
        <span>Find artist</span>
        <input
          type="text"
          onChange={(e) => handleArtistChange(e)}
          id="artistSearch"
          value={artistInput}
          placeholder="Artiest.."
          required
        />
      </label>
      <button className={styles.button} type="submit">Zoek</button>
    </form>
  )
};

export default ArtistSearch;
