import React from 'react';
import styles from './artistsList.module.css';
import { useFetchArtists } from '../../../hooks/useFetchArtists';

const ArtistsList = ({ artistQuery }) => {
  const { artists, loading, error } = useFetchArtists('/artists', '20', 0, artistQuery);

  if (loading) return <div>Loading...</div>
  if (error) return <div>{error}</div>

  return (
    <ul className={styles.artistItems}>
      {
        artists && artists.length > 0
        && artists.map((artist) => (
          <li className={styles.artistItem} key={artist.id}>
            {artist.name}
          </li>
        ))
      }
    </ul>
  );
};

export default ArtistsList;
