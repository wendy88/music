import React, { useState } from 'react';
import styles from './artists.module.css';
import ArtistsList from './ArtistsList/artistsList';
import ArtistSearch from './ArtistSearch/artistSearch';

const Artists = () => {
  const [artistQuery, setArtistQuery] = useState('');

  return (
    <div className={styles.artistPage}>
      <ArtistSearch
        setArtistQuery={setArtistQuery}
      />
      <ArtistsList
        artistQuery={artistQuery}
      />
    </div>
  );
};

export default Artists;
