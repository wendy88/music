import React from 'react';
import styles from './layout.module.css';
import Header from '../Header/header';
import { PlaylistsProvider } from '../../context/playlistsContext';

const Layout = ({ children }) => (
  <div className={styles.layoutWrapper}>
    <PlaylistsProvider>
      <Header />
      <main className={styles.main}>
        {children}
      </main>
    </PlaylistsProvider>
  </div>
);

export default Layout;
