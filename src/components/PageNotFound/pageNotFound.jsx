import React from 'react';

const PageNotFound = () => (
  <div>
    <h1>Please don&apos;t stop the music</h1>
  </div>
);

export default PageNotFound;
