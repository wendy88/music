import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import logo from './logo.png';
import styles from './header.module.css';
import { PlaylistsContext } from '../../context/playlistsContext';

const Header = () => {
  const { playlistsState } = useContext(PlaylistsContext);

  return (
    <header>
      <span className={styles.intro}>hey, hey I wanna be a</span>
      <div className={styles.headerWrap}>
        <Link
          to="/"
        >
          <img src={logo} alt="Rockstars" width={75} />
        </Link>
        <Link
          to="/playlists"
          className={styles.playlistLink}
        >
          My playlists
          (
          {playlistsState.playlists.length}
          )
        </Link>
      </div>
    </header>
  );
};

export default Header;
