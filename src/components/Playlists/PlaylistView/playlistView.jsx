import React, { useContext } from 'react';
import { PlaylistsContext } from '../../../context/playlistsContext';
import styles from './playlistsView.module.css';
import PlaylistItem from '../PlaylistItem/playlistItem';

const PlaylistsView = () => {
  const { playlistsState } = useContext(PlaylistsContext);

  return (
    <div className={styles.playlists}>
      {
        playlistsState.playlists && playlistsState.playlists.length > 0
          ? playlistsState.playlists.map((playlist) => <PlaylistItem key={playlist.id} item={playlist} />)
          : <p>Je hebt nog geen playlists aangemaakt.</p>
      }
    </div>
  );
};

export default PlaylistsView;
