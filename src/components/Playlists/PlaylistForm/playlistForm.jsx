import React, { useContext, useCallback, useState } from 'react';
import { PlaylistsContext } from '../../../context/playlistsContext';
import styles from './playlistForm.module.css';

const PlayListForm = () => {
  const { playlistsDispatch } = useContext(PlaylistsContext);
  const [playlistName, setPlaylistName] = useState('');

  const handlePlaylistChange = useCallback((e) => {
    setPlaylistName(e.target.value);
  }, [setPlaylistName]);

  const submitPlaylistForm = useCallback((e) => {
    e.preventDefault();

    // To do when more time: Only execute when name does not exists yet
    // (URLified version is used as id)
    playlistsDispatch({
      type: 'ADD_PLAYLIST',
      payload: playlistName,
    });

    setPlaylistName('');
  }, [playlistsDispatch, playlistName, setPlaylistName]);

  return (
    <form
      onSubmit={(e) => submitPlaylistForm(e)}
      className={styles.playlistForm}
    >
      <label htmlFor="playlist" className={styles.label}>
        <span>Playlist name</span>
        <input
          type="text"
          id="playlist"
          value={playlistName}
          placeholder="De naam van je playlist.."
          onChange={(e) => handlePlaylistChange(e)}
          required
        />
      </label>
      <button className={styles.button} type="submit">Maak een playlist</button>
    </form>
  );
};

export default PlayListForm;
