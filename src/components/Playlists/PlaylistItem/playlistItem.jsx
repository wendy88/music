import React, { useCallback, useContext } from 'react';
import styles from './playlistItem.module.css';
import { PlaylistsContext } from '../../../context/playlistsContext';

const PlaylistItem = ({ item }) => {
  const { playlistsDispatch } = useContext(PlaylistsContext);
  const { name, id } = item;

  const handleRemovePlaylist = useCallback((e) => {
    playlistsDispatch({
      type: 'REMOVE_PLAYLIST',
      payload: e.target.name,
    });
  }, [playlistsDispatch]);

  return (
    <div className={styles.playlistItem}>
      <p className={styles.name}>{name}</p>
      <button
        className={styles.button}
        type="button"
        name={id}
        onClick={(e) => handleRemovePlaylist(e)}
      >
        x
      </button>
    </div>
  );
};

export default PlaylistItem;
