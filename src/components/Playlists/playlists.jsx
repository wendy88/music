import React from 'react';
import PlaylistsView from './PlaylistView/playlistView';
import PlayListForm from './PlaylistForm/playlistForm';
import styles from './playlists.module.css';

const Playlists = () => (
  <div className={styles.playlistPage}>
    <PlaylistsView />
    <PlayListForm />
  </div>
);

export default Playlists;
