import { useEffect, useState } from 'react';
import axios from 'axios';

export const useFetchArtists = (url = '/artists', limit = 20, page = 0, search) => {
  const [artists, setArtists] = useState([])
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState(null)

  useEffect(() => {
    setLoading(true);
    setError(null);

    const apiUrl = search && search.length > 0
      ? `${url}?q=${search}&_page=${page}&_limit=${limit}`
      : `${url}?page=${page}&_limit=${limit}`;

    const fetchData = async () => {
      try {
        const { data } = await axios(apiUrl);
        setArtists(data);
        setLoading(false);
      } catch (err) {
        setError(err);
        setLoading(false);
      }
    };

    fetchData();
  }, [limit, page, search, url]);

  return { artists, loading, error };
}
