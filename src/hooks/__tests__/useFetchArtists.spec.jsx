import { renderHook, act } from '@testing-library/react-hooks';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { useFetchArtists } from '../useFetchArtists';

describe('useFetchArtists', () => {
  it('fetches artists', async () => {
    const mock = new MockAdapter(axios);
    const mockData = '{ artists: []}';
    const url = '/artists';
    mock.onGet(url).reply(200, mockData);

    const { result, waitForNextUpdate } = renderHook(() => useFetchArtists());

    expect(result.current.loading).toBeTruthy();

    await act(async () => {
      await waitForNextUpdate();
    });

    expect(result.current.loading).toBeFalsy();

    expect(result.current.artists).toEqual([]);
  });
});
